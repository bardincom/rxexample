import UIKit
import RxSwift
import RxCocoa


// MARK: - Model -
struct TargetModel: Codable {
    var name: String?
    var amount: String?
    var date: String?
}

// MARK: - ViewModel -
protocol PEditDepositVM {
    var router: Router! { get set }
    var repository: PRepository! { get set }
    var disposeBag: DisposeBag { get }
    var dataOutput: BehaviorRelay<InfoModel?> { get }
    var actionVM: PublishSubject<EditDepositVM.ActionVM> { get }
    var actionView: PublishSubject<EditDepositVM.ActionView> { get }
    var targetModel: BehaviorRelay<TargetModel?> { get }
    func injectionDidFinish()
}

final class EditDepositVM: PEditDepositVM {
    var router: Router!
    var repository: PRepository!
    var disposeBag = DisposeBag()
    var actionVM: PublishSubject<EditDepositVM.ActionVM> = PublishSubject()
    var actionView: PublishSubject<EditDepositVM.ActionView> = PublishSubject()
    var dataOutput: BehaviorRelay<InfoModel?> = BehaviorRelay(value: nil)
    var targetModel: BehaviorRelay<TargetModel?> = BehaviorRelay(value: nil)
    
    private let networkManager: PNetworkManager
    private let scheduler: PScheduler
                           
    init(networkManager: PNetworkManager = NetworkManager.shared, scheduler: PScheduler = Scheduler()) {
        self.networkManager = networkManager
        self.scheduler = scheduler
    }
    
    func injectionDidFinish() {
        subscribeAction()
        subscribeObservers()
        dataOutput.accept(repository.infoModel)
    }
    
    private func subscribeObservers() {
        targetModel
            .subscribeOn(scheduler.background)
            .observeOn(scheduler.mainAsync)
            .filterNil()
            .filter { [weak self] target in
                if target.name?.isEmpty == false {
                    self?.actionView.onNext(.isEnableNextButton(true))
                    return true
                } else {
                    self?.repository.modelData?.name = nil
                    self?.actionView.onNext(.isEnableNextButton(false))
                    return false
                }
            }
            .subscribe(onNext: { [weak self] target in
                self?.repository.modelData?.name = target.name
                self?.repository.modelData?.date = target.date
                guard let amount = target.amount else { return }
                self?.repository.modelData?.amount = Int(amount)
            }).disposed(by: disposeBag)
    }
    
    private func subscribeAction() {
        actionVM
            .subscribeOn(scheduler.background)
            .observeOn(scheduler.mainAsync)
            .subscribe(onNext: { [weak self] action in
                switch action {
                case .onBack:
                    // Routing back
                case .onSave:
                    // Saving changes
                case .onGallery:
                    // Open new VC
                case .onDelete:
                    // Network request for delete
                }
            }).disposed(by: disposeBag)
    }
}

extension EditDepositVM {
    enum ActionVM {
        case onBack
        case onGallery
        case onSave
        case onDelete
    }
    
    enum ActionView {
        case isEnableNextButton(Bool)
        case isEnableCalendar(Bool)
    }
}

// MARK: - ViewController -

final class EditDepositVC: BaseViewController {
    // MARK: IBOutlet
    @IBOutlet private var vHeader: HeaderEditView!
    @IBOutlet private var vEdit: EditView!
    @IBOutlet private var vButtonContainer: UIView!
    @IBOutlet private var bSave: UIButton!
    @IBOutlet private var bDelete: UIButton!
    @IBOutlet private var lcBottom: NSLayoutConstraint!

    var viewModel: PEditDepositVM!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        bindActions()
        setupData()
        subscribeToShowKeyboardNotifications()
        addGesture()
    }
    
    // MARK: Private methods
    private func setupNavigation() {
        setupCustomNavigationBar(barStyle: .white, isRightBarButtonItem: true, rightBarButtonType: .photo)
    }
    
    private func bindActions() {
        backBarButtonItem.rx.tap.map { _ in .onBack}.bind(to: viewModel.actionVM).disposed(by: viewModel.disposeBag)
        bSave.rx.tap.map { .onSave }.bind(to: viewModel.actionVM).disposed(by: viewModel.disposeBag)
        photoBarButtonItem.rx.tap.map { _ in .onGallery }.bind(to: viewModel.actionVM).disposed(by: viewModel.disposeBag)
        bDelete.rx.tap.map { .onSave }.bind(to: viewModel.actionVM).disposed(by: viewModel.disposeBag)
        
        vHeader.changePhotoObserver.bind(to: viewModel.actionVM).disposed(by: viewModel.disposeBag)
        vEdit.targetObserver.bind(to: viewModel.targetMoneyBox).disposed(by: viewModel.disposeBag)
        vEdit.actionView.bind(to: viewModel.actionView).disposed(by: viewModel.disposeBag)
        
        viewModel.actionView
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] action in
                guard let self = self else { return }
                switch action {
                case .isEnableNextButton(let state):
                    self.bSave.isEnabled = state
                case .isEnableCalendar:
                    self.view.endEditing(true)
                }
            }).disposed(by: viewModel.disposeBag)
    }
    
    private func setupData() {
        viewModel.dataOutput
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] data in
                guard let self = self else { return }
                self.vEdit.fillData(data)
        }).disposed(by: viewModel.disposeBag)
    }
}

private extension EditMoneyBoxNewVC {
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification)
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] notification in
                guard let self = self else { return }
                self.bDelete.isHidden = true
                let keyboardSize = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
                let keyboardHeight = keyboardSize.cgRectValue.height
                self.lcBottom.constant = keyboardHeight
                guard let animationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else { return }
                UIView.animate(withDuration: animationDuration) {
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: viewModel.disposeBag)

        NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification)
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] notification in
                guard let self = self else { return }
                self.bDelete.isHidden = false
                self.lcBottom.constant = 0
                guard let animationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else { return }
                UIView.animate(withDuration: animationDuration) {
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: viewModel.disposeBag)
    }
    
    func addGesture() {
        let tap = UITapGestureRecognizer()
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        tap.rx
            .event
            .subscribe(onNext: { [weak self] _ in
                self?.view.endEditing(true)
            }).disposed(by: viewModel.disposeBag)
    }
}

// MARK: - HeaderEditView -

final class HeaderEditView: UIView {
    @IBOutlet private var vComponent: UIView!
    
    let changePhotoObserver: PublishSubject<EditDepositVM.ActionVM> = PublishSubject()
    private let disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setGesture()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setGesture()
    }
    
    private func setGesture() {
        let onCamera = UITapGestureRecognizer()
        svComponent.addGestureRecognizer(onCamera)
        onCamera.rx.event.map { _ in .onGallery }.bind(to: changePhotoObserver).disposed(by: disposeBag)
    }
}

// MARK: - EditView -
final class EditView: UIView {
    @IBOutlet private var tfTargetName: UITextField!
    @IBOutlet private var tfAmount: UITextField!
    @IBOutlet private var dpCalendar: UIDatePicker!
    @IBOutlet private var switcher: UISwitch!
    
    var targetObserver: BehaviorRelay<TargetModel?> = BehaviorRelay(value: nil)
    var actionView: PublishSubject<EditDepositVM.ActionView> = PublishSubject()
    
    private var targetModel = TargetModel()
    private var disposeBag = DisposeBag()

    override init(frame: CGRect) {
        super.init(frame: frame)
        subscribeData()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        subscribeData()
    }
    
    func fillData(_ data: InfoModel?) {
        // Some code
    }
    
    private func subscribeData() {
        fieldsObserver()
        
        actionView
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] action in
                guard let self = self else { return }
                guard case .isEnableCalendar(let state) = action else { return }
                self.dpCalendar.isHidden = state
                if state {
                    self.targetModel.date = nil
                    self.targetObserver.accept(self.targetModel)
                } else {
                    self.targetDateHelper(self.dpCalendar.date)
                }
            }).disposed(by: disposeBag)
    }
    
    private func fieldsObserver() {
        tfTargetName.rx.value
            .filterNil()
            .distinctUntilChanged()
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.asyncInstance)
                .subscribe(onNext: { [weak self] result in
                    guard let self = self else { return }
                    self.targetModel.name = result
                    self.targetObserver.accept(self.targetModel)
            }).disposed(by: disposeBag)
        
        tfAmount.rx.value
            .filterNil()
            .distinctUntilChanged()
            .subscribeOn(SerialDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.asyncInstance)
                .subscribe(onNext: { [weak self] result in
                    guard let self = self else { return }
                    self.targetModel.amount = result
                    self.targetObserver.accept(self.targetModel)
            }).disposed(by: disposeBag)
        
        switcher.rx.isOn
            .skip(1)
            .map { .isEnableCalendar(!$0) }
            .bind(to: actionView).disposed(by: disposeBag)
    }
    
    private func targetDateHelper(_ date: Date) {
        let dateString = dateService.convertDateToStringWithFormat(date: date, format: .datePiker)
        targetModel.date = dateString
        targetObserver.accept(targetMoneyBox)
    }
}
